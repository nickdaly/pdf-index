#! /usr/bin/env bash
#
# Builds indexes from PDFs
# Copyright (C) 2018, Nick Daly

cleanText="tr -d [:digit:] | tr -d [:punct:] | tr -c [:print:] \" \" | tr -t [:upper:] [:lower:] | sed 's/ \+/\n/g' | egrep \".+\" | fgrep -vxf ~/.google-10000-english.txt | sort -u"
commonWords="sort | uniq -c | sort -nr | head -50 | awk '{print \$2}'"

for mypdf in "$@"
do
    if [[ "$mypdf" != *"pdf" ]]
    then
	continue
    fi

    mypdfName=${mypdf##*/}
    mypdfTmp="/tmp/${mypdfName}"

    rm $mypdfTmp-wordlist.txt-* &> /dev/null
    rm $mypdf-index.txt &> /dev/null

    pages=`pdfinfo $mypdf | awk '/Pages:/ { print $2 }'`

    # build word lists
    for i in `seq 1 $pages`
    do
	eval "pdftotext -f $i -l $i $mypdf - | $cleanText > $mypdfTmp-wordlist.txt-$i"
    done

    # find common words
    eval "cat $mypdfTmp-wordlist.txt-* | $commonWords > $mypdfTmp-wordlist.txt"

    # find occurrences of common words
    #
    # TODO: rewrite this in awk.
    #     awk $mypdf-wordlist.txt "BEGIN={ make this input an array }
    #     for each page in pdf: add page num if words match."
    echo $mypdfName > $mypdf-index.txt
    echo "" >> $mypdf-index.txt
    python3 index-words $mypdf >> $mypdf-index.txt
done
