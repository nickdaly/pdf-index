#! /usr/bin/env python3
#
# Inverts lists of common words into an index.
# Copyright (C) 2018, Nick Daly

import shlex
import subprocess
import sys

mypdfpath = sys.argv[1]
mypdf = "/tmp/" + sys.argv[1].rpartition("/")[2]
words = dict()

with open(mypdf + "-wordlist.txt") as wordfile:
    for line in wordfile:
        words[line.strip()] = list()

# select second piece of stdout's "Pages" line:
#
#    JavaScript:     no
#    Pages:          15
#                     ^-- this
#
# pdfinfo | grep "Pages:" | awk '{ print $2 }'
pages = [x.split()[1] for x in
         subprocess.Popen(["pdfinfo", mypdfpath],
                          universal_newlines=True,
                          stdout = subprocess.PIPE).communicate()[0].splitlines()
         if "Pages:" in x][0]
pages = int(pages)

for i in range(1, pages):
    with open(mypdf + "-wordlist.txt-{}".format(i)) as wordfile:
        for j in wordfile:
            try:
                words[j.strip()].append(i)
            except KeyError:
                pass

print("\n".join(
    ["{0:20} {1}".format(k,
                     ", ".join([str(v) for v in words[k]]))
                 for k in sorted(words)]))
